# PulseGenerator

This Arduino project pulses an output pin (GPIO 23) for a given time. The high and low durations can be set indivually over UART interface. The repeations of that cacle can be set as well.

## Hardware

The code is created for the *Node MCU 32S* Board with Arduino bootloader.
## Interface

The high and low durations as well as the number of repeatitions can be set via uart. Simply connect a UART-dognle to the board and your PC and connect a serial terminal to that COM-Port with a baud rate of 115200.

A configuration string schould be as the following "[high pulse duration in ms];[low pulse duration in ms];[number of repitions]\n". Be sure to send that ASCII string with an terminating '\n'! The pulse is started as soon you send the string. Additionally the pulse can be triggered with the onboard button with the last thet parameters. The onboard LED has the same state as the output pin.
