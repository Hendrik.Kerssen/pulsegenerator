/*
  This Arduino project pulses an output pin for a given time.
  The high and low durations can be set indivually over UART interface.
  The repeations of that cacle can be set as well.
*/

#include <Arduino.h>

// Pin definitions
const int BTN = 0;
const int LED = 2;
const int PIN_23 = 23;

// Global variables
int high_duration_ms = 500;
int low_duration_ms = 500;
int periods = 1;
bool periodEnable = false;
int periodCounter = 0;
bool btnToggle = false;

void setup() {
    // initialize serial:
    Serial.begin(115200);

    // wait for initialization
    delay(100);

    // print welcome screen
    Serial.printf("\niotec\nPulseGenerator\n");

    // set the pins as outputs:
    pinMode(BTN, INPUT);
    pinMode(LED, OUTPUT);
    pinMode(PIN_23, OUTPUT);
}

void loop() {
    // if there's any serial input available, read it:
    while (Serial.available() > 0) {
        // look for the next valid integer in the incoming serial stream and parse it as integer
        high_duration_ms = Serial.parseInt();

        // check if we reached the first ';'
        if (Serial.read() == ';') {
            // look for the next valid integer in the incoming serial stream and parse it as integer
            low_duration_ms = Serial.parseInt();

            // check if we reached the second ';'
            if (Serial.read() == ';') {
                // look for the next valid integer in the incoming serial stream and parse it as integer
                periods = Serial.parseInt();
            }
        }

        // look for the newline. That's the end of your message
        if (Serial.read() == '\n') {
            // print the received variables
            Serial.printf("-> %i x %ims high and %ims low\n", periods, high_duration_ms, low_duration_ms);

            // Check if we received negative duration for high duration
            if (high_duration_ms < 0) {
                // Set duration to 0
                high_duration_ms = 0;
            }

            // Check if we received negative duration for low duration
            if (low_duration_ms < 0) {
                // Set duration to 0
                low_duration_ms = 0;
            }

            // Check if we received 0 periods
            if (!periods) {
                // Set periods to at least 1
                periods = 1;
            }

            // Copy the received periods
            periodCounter = periods;
        }
    }

    // Check if button is pressed the first time
    if ((digitalRead(BTN) == false) && (!btnToggle)) {
        Serial.printf("BTN pressed\n");

        // print the last received variables
        Serial.printf("-> %i x %ims high and %ims low\n", periods, high_duration_ms, low_duration_ms);

        // Copy the received periods
        periodCounter = periods;
    }

    // Check if we have to perform at least one cycle
    if (periodCounter > 0) {
        // print remaining cycles
        Serial.printf("remaining cycles: %i\n", periodCounter);

        // Perform cycle
        pulse();

        // Reduce remaining cycles
        periodCounter--;

        // Check if there are no more cycles left
        if (periodCounter <= 0) {
            // enable button after all cycles
            btnToggle = false;
        } else {
            // disable button for cycles
            btnToggle = true;
        }
    }
}

// Perform one cycle
void pulse() {
    // Set output pin and onboard led high
    digitalWrite(LED, true);
    digitalWrite(PIN_23, true);

    // wait given high duration
    delay(high_duration_ms);

    //// Set output pin and onboard led high
    digitalWrite(LED, false);
    digitalWrite(PIN_23, false);

    // wait given low duration
    delay(low_duration_ms);
}
